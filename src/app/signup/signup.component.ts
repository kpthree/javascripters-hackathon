import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { SignupService } from "./signup.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  public signupForm;
  
  constructor(
    private fb: FormBuilder,
    private signupService: SignupService
  ) {
    this.createSignUpForm();
   }

  ngOnInit() {
    
  }

  createSignUpForm = () => {
    this.signupForm = this.fb.group({
      email:['', Validators.required],
      phoneNumber:['', Validators.required]
    })
  }

  signUp = () => {
    const signupPayload = this.signupService.getSignupPayload(this.signupForm.value)
    
    this.signupService.signUp(signupPayload)
    .subscribe(
      data=> console.log("Data is", data),
      err => console.log("Error is", err)
    )
  }

}
