import {Component, OnInit} from '@angular/core';
import {AppService} from "./app.service";
import {AppSharedService} from "./common/shared-service/app-shared.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [AppService, AppSharedService]
})
export class AppComponent implements OnInit{

  appData: any;

  constructor(public appService: AppService, private appSharedService: AppSharedService ) {}

  ngOnInit() {
    this.getAllData();
  }

  getAllData() {
    this.appService.getAppData().subscribe(
      (data: any) => {
        this.appData = data;
        this.appSharedService.appDataRecieved(data);
      }, (error: any) => {
        console.log(error);
      }
    );
  }
}
