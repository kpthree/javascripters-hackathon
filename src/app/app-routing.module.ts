import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SubscribeComponent} from "./subscribe/subscribe.component";
import { SignupComponent } from './signup/signup.component';
import {AdminComponent} from "./admin/admin.component";
import {LoginComponent} from "./common/login/login.component";


const appRoutes: Routes = [
  {path: 'signup', component: SignupComponent},
  {path: '', redirectTo: '/subscribe', pathMatch: 'full'},
  {path: 'subscribe', component: SubscribeComponent, pathMatch: 'full'},
  {path: 'admin', component: AdminComponent, pathMatch: 'full'},
  {path: 'login', component: LoginComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
