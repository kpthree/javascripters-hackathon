import {Inject, Injectable} from '@angular/core';
import {URLConstants} from '../common/constants/URL.constant';
import 'rxjs/add/operator/map';
import {Http, Headers, Response} from '@angular/http';

@Injectable()
export class AppService {

  crawlLinkUrl: string;

  constructor(@Inject(URLConstants) URLConstants: URLConstants, private http: Http) {
    this.crawlLinkUrl = URLConstants.crawlLinkURL;
  }

  crawlLink(url: any) {
    const body = url.encodeURIComponent;
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.get(this.crawlLinkUrl + body, {headers: headers})
      .map(res => res.json());
  }

}
