import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from "@angular/http";
import {ReactiveFormsModule} from "@angular/forms";

// NGX Bootstrap Imports
import {ModalModule} from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import {URLConstants} from "./common/constants/URL.constant";
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { AppRoutingModule } from './app-routing.module';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { SignupComponent } from './signup/signup.component';
import { SignupService } from './signup/signup.service';
import { HttpClientService } from './common/shared-service/http-client.service';
import { AdminComponent } from './admin/admin.component';
import { NotificationsComponent } from './admin/notifications/notifications.component';
import { MessagingService } from './common/shared-service/messaging.service'
// Angular fire
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireAuth } from 'angularfire2/auth';
//
import {LoginComponent} from "./common/login/login.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SubscribeComponent,
    SignupComponent,
    AdminComponent,
    NotificationsComponent,
    LoginComponent, 
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ModalModule.forRoot(),
    AngularFireModule.initializeApp({ 
      apiKey: "AIzaSyBmwpWUsEGMKhgKV_pe1zCxfhaQ-V4iu48",
      databaseURL: 'https://akshaytestingjs.firebaseio.com',
      messagingSenderId: '76008323591'
    }),
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ],
  providers: [
    URLConstants,
    SignupService,
    HttpClientService,
    MessagingService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
