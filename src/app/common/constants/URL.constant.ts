// Define Application URLs Here

import {Injectable} from '@angular/core';

@Injectable()
export class URLConstants {

  // Base URL
  _URL: string;

  // Tags URL
  appData: string;
  signup: string;

  // Crawl Link
  crawlLinkURL: string;

  constructor() {

    this._URL = 'https://javascripters.glitch.me/';

    // Tags
    this.appData = this._URL + 'tags';
    this.signup = this._URL + 'signup';
    // Crawl Link URL
    this.crawlLinkURL = this._URL + 'c/';
  }

}
