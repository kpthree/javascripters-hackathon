import { Component, OnInit } from '@angular/core';
import {AppSharedService} from "../common/shared-service/app-shared.service";
import { MessagingService } from "./../common/shared-service/messaging.service";

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
})
export class SubscribeComponent implements OnInit {

  tags: any[] = [];
  tagArray: any[] = [];
  message;
  model = {
    tags: [
      'React Js', 'Angular Js', 'Meteor Js'
    ]
  };

  constructor(
    public appSharedService: AppSharedService,
    private msgService: MessagingService) {
    this.appSharedService.getAppData.subscribe(
      (data: any) => {
        this.tags = data.pages.subscribe.tags;
      }
    );
  }

  ngOnInit() {
    this.msgService.getPermission()
    this.msgService.receiveMessage()
    this.message = this.msgService.currentMessage
  }

  addTagsToSubscribe(tags: any) {
    const tag = {
      tag: tags,
      selected: true,
    };
    this.tagArray.push(tag);
    this.tags = Array.from(new Set(this.tagArray));
    console.log(this.tags);
  }

}
