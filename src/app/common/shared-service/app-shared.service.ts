import {Injectable, EventEmitter} from '@angular/core';
import 'rxjs/Rx';

@Injectable()
export class AppSharedService {
  getAppData: EventEmitter<any> = new EventEmitter<any>();

  appData = {};

  appDataRecieved(data: any) {
    this.appData = data;
    this.getAppData.next(this.appData);
  }
}

