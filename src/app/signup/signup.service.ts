import { Injectable } from '@angular/core';
import { URLConstants } from "./../common/constants/URL.constant";
import { Http } from '@angular/http';

@Injectable()
export class SignupService {

  constructor(
    private http: Http,
    private urls: URLConstants
  ) { }

  signUp = (payload) => {
    return this.http.get(this.urls.signup, payload);
  }

  getSignupPayload = (formValues) => {
    return formValues;
  }

}
