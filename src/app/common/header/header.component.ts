import { Component, OnInit } from '@angular/core';
import {AppSharedService} from "../shared-service/app-shared.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  headerData: any;

  constructor(private appSharedService: AppSharedService) {
    this.appSharedService.getAppData.subscribe(
      (data: any) => {
        this.headerData = data.header;
      }, (error: any) => {
        console.log(error);
      }
    );
  }

  ngOnInit() {

  }

}
