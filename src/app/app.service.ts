import {Inject, Injectable} from '@angular/core';
import {URLConstants} from "./common/constants/URL.constant";
import 'rxjs/add/operator/map';
import {Http, Headers, Response} from '@angular/http';

@Injectable()
export class AppService {

  appDataURL: string;

  constructor(@Inject(URLConstants) URLConstants: URLConstants, private http: Http) {
    this.appDataURL = URLConstants.appData;
  }

  getAppData() {
    return this.http.get(this.appDataURL)
      .map(res => res.json());
  }

}
